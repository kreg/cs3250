datatype ('a, 'b) semipair = P of 'a * 'b | S of 'a;

fun pairCount nil = 0
| pairCount ((P(a, b))::r) = 1 + pairCount r
| pairCount (S(a)::r) = pairCount r;

fun singCount nil = 0
| singCount (S(a)::r) = 1 + singCount r
| singCount ((P(a, b))::r) = singCount r;

fun hasKey nil a = false
| hasKey ((P(a, b))::r) x = a = x orelse hasKey r x
| hasKey (S(a)::r) x = a = x orelse hasKey r x;

fun getItem nil x = NONE
| getItem ((P(a, b))::r) x = if x = a then SOME(P(a, b)) else getItem r x
| getItem (S(a)::r) x = if x = a then SOME(S(x)) else getItem r x;

fun getValue nil x = NONE
| getValue ((P(a, b))::r) x = if x = a then SOME(b) else getValue r x
| getValue (S(a)::r) x = getValue r x;

fun getKeys nil = nil
| getKeys (S(a)::r) = a::getKeys r
| getKeys ((P(a, b))::r) = a::getKeys r;

fun getValues nil= nil
| getValues (S(a)::r)= getValues r
| getValues ((P(a, b))::r) = b::getValues r;

fun insertItem nil x = [x]
| insertItem a (S(x)) = if hasKey a x then a else (S(x))::a
| insertItem a (P(x, y)) = if hasKey a x then a else (P(x, y))::a;

fun removeItem nil x = []
| removeItem (S(a)::r) x = if x = a then r else S(a)::removeItem r x
| removeItem (P(a, b)::r) x = if x = a then r else P(a, b)::removeItem r x;

(*test cases that were given*)

val stuff = [(P (1,"one")), S 2];
pairCount stuff;
singCount stuff;
getItem stuff 1;
getItem stuff 10;
getValue stuff 1;
getValue stuff 10;
getKeys stuff;
getValues stuff;
removeItem stuff 1;
insertItem stuff (P (3,"three"));
stuff;

(*test cases that were given*)

val stuff = [(P (#"A","A is for A'IGHT")), P(#"B", "B is for BIATCH!"), P(#"C", "C is for CUZ"), S(#"D")];
pairCount stuff;
singCount stuff;
getItem stuff #"A";
getItem stuff #"Z";
getValue stuff #"A";
getValue stuff #"Z";
getKeys stuff;
getValues stuff;
removeItem stuff #"A";
insertItem stuff (P (#"H", "H is for HOMEY"));
stuff;
