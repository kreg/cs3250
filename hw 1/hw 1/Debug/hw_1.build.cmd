set PATH=C:\D\DMD2\windows\bin;C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\\bin;%PATH%
set DMD_LIB=C:\D\dmd2\windows\lib
dmd -g -debug -X -Xf"Debug\hw_1.json" -I"C:\D\DMD2\\windows\import" -deps="Debug\hw_1.dep" -of"Debug\hw_1.exe_cv" -map "Debug\hw_1.map" -L/NOMAP main.d
if errorlevel 1 goto reportError
if not exist "Debug\hw_1.exe_cv" (echo "Debug\hw_1.exe_cv" not created! && goto reportError)
echo Converting debug information...
"C:\Program Files (x86)\VisualD\cv2pdb\cv2pdb.exe" -D2 "Debug\hw_1.exe_cv" "Debug\hw_1.exe"
if errorlevel 1 goto reportError
if not exist "Debug\hw_1.exe" (echo "Debug\hw_1.exe" not created! && goto reportError)

goto noError

:reportError
echo Building Debug\hw_1.exe failed!

:noError
