module main;

import std.stdio;
import std.file;
import std.string;

void main()
{
	int[int][string] mainArray;
	foreach (int lineNum, string line; lines(stdin))
		foreach(word; split(line))
			mainArray[word][lineNum +1]++;
	foreach (stg; mainArray.keys.sort)
	{
		write(stg ~ ": ");
		foreach (x; mainArray[stg].keys.sort)
			writef("%d:%d, ", x, mainArray[stg][x]);
		writeln();
	}
	readln();
}
