set PATH=C:\D\DMD2\windows\bin;C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\\bin;%PATH%
set DMD_LIB=C:\D\dmd2\windows\lib
dmd -g -debug -X -Xf"Debug\Deque.json" -I"C:\D\DMD2\\windows\import" -deps="Debug\Deque.dep" -of"Debug\Deque.exe_cv" -map "Debug\Deque.map" -L/NOMAP -unittest main.d
if errorlevel 1 goto reportError
if not exist "Debug\Deque.exe_cv" (echo "Debug\Deque.exe_cv" not created! && goto reportError)
echo Converting debug information...
"C:\Program Files (x86)\VisualD\cv2pdb\cv2pdb.exe" -D2 "Debug\Deque.exe_cv" "Debug\Deque.exe"
if errorlevel 1 goto reportError
if not exist "Debug\Deque.exe" (echo "Debug\Deque.exe" not created! && goto reportError)

goto noError

:reportError
echo Building Debug\Deque.exe failed!

:noError
