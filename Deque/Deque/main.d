//module main;

import std.stdio;

struct Deque(T)
{
public:
	void push_front(T toPush)
	{
		frontArray.length += 1;
		frontArray[$-1] = toPush;
	}
	void push_back(T toPush)
	{
		backArray.length += 1;
		backArray[$-1] = toPush;
	}
	T pop_front()
	in { assert(size() > 0); }
	body
	{
		if (frontArray.length > 0)	
		{
			T temp;
			temp = frontArray[$-1];
			frontArray = frontArray[0..$-1];
			return temp;
		}
		else if (backArray.length == 1)
			return pop_back();
		else 
		{
			resize();
			return pop_front();
		}
	}
	T pop_back()
	in { assert(size() > 0); }
	body
	{
		if (backArray.length > 0)
		{
			T temp;
			temp = backArray[$-1];
			backArray = backArray[0..$-1];
			return temp;
		}
		else if (frontArray.length == 1)
			return pop_front();
		else
		{
			resize();
			return pop_back();		
		}
	}
	ref T at(uint pos)
	in { assert(pos <= size()); }
	body	
	{ 
		if (pos < frontArray.length)
			return frontArray[$ - (1 + pos)];
		else return backArray[pos-frontArray.length];
	}
	ref T front()
	in		{ assert(size() > 0); }
	body	
	{ 
		return at(0);
	}
	ref T back() 
	in		
	{ assert(size() > 0); } 
	body	
	{ 
		return at(size()-1);
	} 

	uint size()	
	{ 
		return frontArray.length + backArray.length;	
	}
private:
	T[] frontArray, backArray;
	void dump()
	{
		foreach_reverse (s; frontArray)
			writeln(s);
		foreach (s; backArray)
			writeln(s);
	}
	void resize()
	{
		writeln("resizing");
		if (frontArray.length == 0)
		{
			frontArray = backArray[0..backArray.length/2];
			backArray = backArray[backArray.length/2..$];
		}else
		{
			backArray = frontArray[0..frontArray.length/2];
			frontArray = frontArray[frontArray.length/2..$];
		}
	}
}

unittest {
    Deque!(int) d;
    d.push_front(1);
    d.push_back(2);
    assert(d.at(0) == 1);
    assert(d.at(1) == 2);
    assert(d.size() == 2);
    d.push_front(3);
    d.push_back(4);
    assert(d.back() == 4);
    assert(d.front() == 3);
    d.at(1) = 10;
    d.front() = 30;
    d.back() = 40;
    assert(d.back() == 40);
    assert(d.front() == 30);
    assert(d.at(1) == 10);

    d.dump();
    writeln(d.pop_front());
    writeln(d.pop_front());
    writeln(d.pop_front());
    writeln(d.pop_front());
    d.dump();

    Deque!(string) d2;
    d2.push_front("one");
    d2.push_back("two");
    assert(d2.at(0) == "one");
    assert(d2.at(1) == "two");
    assert(d2.size() == 2);
    d2.push_front("three");
    d2.push_back("four");
    assert(d2.back() == "four");
    assert(d2.front() == "three");
    d2.at(1) = "ten";
    d2.front() = "thirty";
    d2.back() = "forty";
    assert(d2.back() == "forty");
    assert(d2.front() == "thirty");
    assert(d2.at(1) == "ten");

    d2.dump();
    writeln(d2.pop_back());
    writeln(d2.pop_back());
    writeln(d2.pop_back());
    writeln(d2.pop_back());
    d2.dump();

    writeln("unit test passed");
	readln();
}

void main(){}
