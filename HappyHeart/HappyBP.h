#include <iostream>
#include "TimeStamp.h"

class HappyBP
{
private:
	static const int MAX_INTERVAL = 600;//largest amount of time to go without receiving a reading before going into alarm state (in seconds)
	string alarm;
	bool hasAlarm;
	int alarmLevel;
	TimeStamp lastTime;
public:
	HappyBP(): hasAlarm(false), alarm(""), lastTime(0), alarmLevel(0) { }
	operator bool() { return !hasAlarm; }
	int AlarmLevel()
	{
		return alarmLevel;
	}
	string alarmStatus() { return alarm; }
	int getAlarmLevel() { return alarmLevel; }
	void ReadData()
	{
		lastTime.add(10);
		if (lastTime > MAX_INTERVAL)
		{
			alarmLevel = 1;
			hasAlarm = true;
			alarm = "Warning, it has been over ten minutes since the last blood pressure reading!";
		}
	}
	void ReadData(int systolic, int diastolic)
	{
		if (systolic > 230 || systolic < 0 || diastolic > 150 || diastolic < 0 || systolic <= diastolic)//impossible blood pressure values, alarm level 1
		{
			alarmLevel = 1;
			hasAlarm = true;
			alarm = "Error, impossible blood pressure input! (" +allToString(systolic) + '/' + allToString(diastolic) + ')';
		}
		else if (systolic < 50 || diastolic < 33)//blood pressure dangerously, low alarm level 3
		{
			alarmLevel = 3;
			hasAlarm = true;
			alarm = "Warning, blood pressure is dangerously low! (" + allToString(systolic) + '/' + allToString(diastolic) + ')';
		}
		else if (systolic < 70 || diastolic < 40)//blood pressure moderately low, alarm level 2
		{
			alarmLevel = 2;
			hasAlarm = true;
			alarm = "Warning, blood pressure is significantly low! (" + allToString(systolic) + '/' + allToString(diastolic) + ')';
		}
		else if (systolic > 200 || diastolic > 120)//blood pressure moderately high, alarm level 2
		{
			alarmLevel = 2;
			hasAlarm = true;
			alarm = "Warning, blood pressure is significantly high! (" + allToString(systolic) + '/' + allToString(diastolic) + ')';
		}
		else if (systolic > 150 || diastolic > 90)//blood pressure slightly high, alarm level 1
		{
			alarmLevel = 1;
			hasAlarm = true;
			alarm = "Warning, blood pressure is slightly high (" + allToString(systolic) + '/' + allToString(diastolic) + ')';
		}
		else//blood pressure reading was okay
		{
			alarmLevel = 0;
			hasAlarm = false;
			alarm = "";
		}
		lastTime = 0;
	}
};