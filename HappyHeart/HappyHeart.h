#include "HappyBP.h"
#include "HappyOxygen.h"
#include "HappyPulse.h"
#include <istream>
#include <fstream>
class HappyHeart
{
private:
	HappyBP BP;
	HappyOxygen oxygen;
	HappyPulse pulse;
	TimeStamp curTime;
	istream* is;
	ostream *os;
	int pulseReading, BPReading1, BPReading2;
	double oxygenReading;
	static const int PULSE = 0;
	static const int OXYGEN = 1;
	static const int BLOODPRESSURE = 2;

	template <class T>
	bool readT(T& dest)
	{
		T input;
		if (*is >> input)
			dest = input;
		else//the rest of the line is bad, throw it away and prepare the stream for next input
		{
			//ClearLine();
			return false;
		}
		return true;
	}
	void ClearLine()
	{
		char throwAway[1000];//behavior is undefined if line is more than 1000 chars long
		is->clear();
		is->getline(throwAway, 1000, '\n');
	}
public:
	HappyHeart(istream& istr, ostream& ostr) : is(&istr), os(&ostr)
	{
		readData();
	}
	void readData()
	{
		char validTester;
		while (*is >> validTester)
		{
			is->putback(validTester);

			string currentReading = "";

			if (!readT(pulseReading))//input wasn't a number
			{
				(*os) << "Error, ignoring invalid input!\n";
				ClearLine();
			}
			else
			{
				curTime.add(10);
				pulse.readData(pulseReading);
				currentReading += allToString(pulseReading);
				if (!readT(oxygenReading)) //only pulse data was available
				{
					oxygen.readData();//oxygen and BP didn't get readings
					BP.ReadData();
				}
				else//there are at least two readings, but maybe more
				{
					if (!readT(BPReading1))//then there was only an oxygen reading
					{
						oxygen.readData(oxygenReading);
						currentReading += " " + doubleToString(oxygenReading);
						BP.ReadData();
					}
					else if (!readT(BPReading2))//the oxygen that got read in was probably actually
												//the first blood pressure reading
					{
						//check to see if the oxygen reading was a decimal number
						if (oxygenReading - (double)((int)oxygenReading))//weird logic, but checking
													//to see if the oxygenReading had anything past the
													//decimal place. True if decimal, false if int.
						{//true means it actually was an oxygen reading, and BP should be ignored
							oxygen.readData(oxygenReading);
							currentReading += " " + doubleToString(oxygenReading);
							BP.ReadData();
						}
						else//no oxygen reading, but BP should be read in
						{
							oxygen.readData();
							BP.ReadData((int)oxygenReading, BPReading1);
							currentReading += " " + allToString(oxygenReading) + "/" + allToString(BPReading1);
						}
					}
					else//all three readings were successfully read
					{
						oxygen.readData(oxygenReading);
						BP.ReadData(BPReading1, BPReading2);
						currentReading += " " + allToString(oxygenReading) + " " + allToString(BPReading1) + "/" + allToString(BPReading2);
					}
				}
				ClearLine();
				int alarmLevel = 0;
				int curAlarm = 0;
				if (!pulse) 
				{
					curAlarm = PULSE;
					alarmLevel = pulse.AlarmLevel();
				}
				if (!oxygen && oxygen.AlarmLevel() > alarmLevel)
				{
					curAlarm = OXYGEN;
					alarmLevel = oxygen.AlarmLevel();
				}
				if (!BP && BP.AlarmLevel() > alarmLevel)
				{
					curAlarm = BLOODPRESSURE;
					alarmLevel = BP.AlarmLevel();
				}
				*os << currentReading << "\t";
				*os << curTime.toString() << "\t";
				if (curAlarm == PULSE)
				{
					*os << pulse.AlarmLevel() << "\t" << pulse.alarmStatus() << endl;
				}
				if (curAlarm == OXYGEN)
				{
					*os << oxygen.AlarmLevel() << "\t" <<  oxygen.alarmStatus() << endl;
				}
				if (curAlarm == BLOODPRESSURE) 
				{
					*os << BP.AlarmLevel() << "\t" <<  BP.alarmStatus() << endl;
				}
			}
		}
	}
	~HappyHeart(){}
};