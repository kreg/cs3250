#include <string>
#include <iomanip>
#include <sstream>
using namespace std;

template <class T>
string allToString(T s)
{
	stringstream out;
	out  << s;
	return out.str();
}
string doubleToString(double s)
{
	stringstream out;
	out << std::fixed << std::setprecision (1) << s;
	return out.str();
}
int stringToInt(string s)
{
	stringstream out;
	out << s;
	int retval;
	out >> retval;
	return retval;
}