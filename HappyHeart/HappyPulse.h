#include <string>
using namespace std;

class HappyPulse
{
private:
	string alarm;
	bool hasAlarm;
	int alarmLevel, lastAlarm, lastReading;
	void repeatWarning()
	{
		if (lastReading < 20) 
		{
			alarm = "Warning, last pulse reading was dangerously low! (" + allToString(lastReading) + ')';
			alarmLevel = 3;
			return;
		}
		else if (lastReading > 170) 
		{
			alarm = "Warning, last pulse reading was dangerously high! (" + allToString(lastReading) + ')';
			alarmLevel = 3;
		}
		else if (lastReading < 40) 
		{
			alarm = "Warning, last pulse reading was moderately low (" + allToString(lastReading) + ')';
			alarmLevel = 2;
		}
		else if (lastReading > 130) 
		{
			alarm = "Warning, last pulse reading was moderately high (" + allToString(lastReading) + ')';
			alarmLevel = 2;
		}
		else if (lastReading > 110) 
		{
			alarm = "Warning, last pulse reading was slightly high (" + allToString(lastReading) + ')';
			alarmLevel = 1;
		}
	}
public:
	HappyPulse(): alarm(""), hasAlarm(false), alarmLevel(0), lastAlarm(0), lastReading(80) {}//set lastReading initially to any non-alarm number
	operator bool() { return !hasAlarm; }
	int AlarmLevel()
	{
		return alarmLevel;
	}
	string alarmStatus()
	{
		return alarm;
	}
	void readData(int reading)
	{
		if (reading < 0 || reading > 210) //invalid data
		{
			alarm = "Error, invalid pulse data, please check equipment! (" + allToString(reading) + ')';
			alarmLevel = 1;
			if (lastAlarm > 1) repeatWarning();//override this reading's assignments if necessary
			hasAlarm = true;
			lastAlarm = 0;
			lastReading = 80; //reset lastReading to any valid input, as we don't want this error to propogage with the repeatWarning function
		}
		else if (reading < 20 || reading > 170)
		{
			if (reading > 170) alarm = "Warning, pulse level dangerously high! (" + allToString(reading) + ')';
			else alarm = "Warning, pulse level dangerously low! (" + allToString(reading) + ')';
			if (reading == 0) alarm = "Danger! Patient has entered Cardiac Arrest!";
			hasAlarm = true;
			alarmLevel = lastAlarm = 3;
			lastReading = reading;
		}
		else if (reading < 40 || reading > 130)
		{
			if (reading < 40)
				alarm = "Warning, pulse level moderately low (" + allToString(reading) + ')';
			else alarm = "Warning, pulse level moderately high (" + allToString(reading) + ')';
			alarmLevel = 2;
			if (lastAlarm > 2) repeatWarning();//override if necessary
			hasAlarm = true;
			lastAlarm = 2;
			lastReading = reading;
		}
		else if (reading > 110)
		{
			alarm = "Warning, pulse level slightly high (" + allToString(reading) + ')';
			alarmLevel = 1;
			if (lastAlarm > 1) repeatWarning();//override if necessary
			hasAlarm = true;
			lastAlarm = 1;
			lastReading = reading;
		}
		else //otherwise, this reading was okay
		{
			alarm = "";
			alarmLevel = 0;
			hasAlarm = false;
			if (lastAlarm > 1)
			{
				repeatWarning();//override if necessary
				hasAlarm = true;
			}
			lastAlarm = 0;
			lastReading = reading;
		}
	}
};