#include "TimeStamp.h"
using namespace std;

class HappyOxygen
{
private:
	static const int NUMBERS_IN_AVERAGE = 6;
	string alarm;
	int alarmLevel;
	bool validAverage, hasAlarm;
	double readings[NUMBERS_IN_AVERAGE], average, lastReading;
	TimeStamp lastTime;
	void calcAverage()
	{
		average = 0.0;
		validAverage = true;
		for (int i = 0; i < NUMBERS_IN_AVERAGE; ++i)
		{
			if (readings[i] == -1.0)
			{
				validAverage = false;
				break;
			}
			average += readings[i];
		}
		average = average / NUMBERS_IN_AVERAGE;
	}
	void updateReadings(double toPush)
	{
		for (int i = 0; i < NUMBERS_IN_AVERAGE; ++i)
		{
			if (i == NUMBERS_IN_AVERAGE -1)
				readings[i] = toPush;
			else readings[i] = readings[i+1];
		}
	}
	void setAlarmStatus()
	{
		calcAverage();
		if (!validAverage)//low alarm
		{
			alarm = "Not enough data to calculate oxygen levels";
			alarmLevel = 1;
			hasAlarm = true;
		}
		else//check to see if other alarms need to be raised
		{
			if (average < 50)//highest alarm
			{
				alarm = "Warning, oxygen levels dangerously low! (" + doubleToString(average) + ")";
				alarmLevel = 3;
				hasAlarm = true;
			}
			else if (average < 80)//medium alarm
			{
				alarm = "Warning, oxygen levels moderately low (" + doubleToString(average) + ")";
				alarmLevel = 2;
				hasAlarm = true;
			}
			else if (average < 85)//low alarm
			{
				alarm = "Oxygen levels slightly low (" + doubleToString(average) + ")";
				alarmLevel = 1;
				hasAlarm = true;
			}
			else//no alarm required
			{
				alarm = "";
				alarmLevel = 0;
				hasAlarm = false;
			}
		}
	}
public:
	HappyOxygen(): validAverage(false), alarm(""), alarmLevel(0), hasAlarm(false), average(0.0), lastReading(-1.0)
	{
		for (int i = 0; i < NUMBERS_IN_AVERAGE; ++i)
			readings[i] = -1.0;
	}
	operator bool() { return !hasAlarm; }
	int AlarmLevel()
	{
		return alarmLevel;
	}
	string alarmStatus() { return alarm; }
	void readData()
	{
		if (lastTime >= 30)//haven't received a status in over 30 seconds
		{
			updateReadings(-1.0);
		}
		else
		{
			updateReadings(lastReading);
		}
		setAlarmStatus();
		lastTime.add(10);
	}
	void readData(double input)
	{
		if (input <= 0.0 || input >= 100.0)//impossible input
		{
			alarm = "Error, invalid oxygen levels. Please check equipment!";
			alarmLevel = 1;
			hasAlarm = true;
			updateReadings(-1);
		}
		else 
		{
			updateReadings(input);
			lastReading = input;
			setAlarmStatus();
			lastTime.reset();
		}
	}
};
/*
Oxygen:

The oxygen level is based on a one minute moving average of the oxygen level.
1.Average below 50% -- highest
2.Average below 80% -- medium
3.Average below 85% -- low
4.If the reading is missing for 30 seconds (3 consecutive readings), this is a low alarm. It usually indicates that the reading device fell off the patientís finger. (This invalidates the moving average.) (If the data is missing for one or two readings, assume that the value is the same as the last reading.)
5.If the oxygen level average is displayed, display it with one digit to the right of the decimal point (1/10th accuracy)
6.Levels of 0% or less, and 100% or higher, are impossible.

*/