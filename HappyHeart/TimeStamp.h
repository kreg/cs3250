#pragma once;
#include <string>
#include "Utility.h"
using namespace std;

class TimeStamp
{
private:
	unsigned long days, hours, minutes, seconds, timeInSeconds;
	void setTimes()
	{
		seconds = timeInSeconds;
		minutes = seconds/60;
		hours = minutes/60;
		days = hours/24;
		seconds = seconds%60;
		minutes = minutes%60;
		hours = hours%24;
	}
public:
	TimeStamp() : hours(0), minutes(0), seconds(0), days(0), timeInSeconds(0) {}
	TimeStamp(unsigned long startTime)
	{
		timeInSeconds = seconds = startTime;
		setTimes();
	}
	inline bool operator >(const TimeStamp& toCompare) const { return timeInSeconds > toCompare.timeInSeconds; }
	inline bool operator >=(const TimeStamp& toCompare) const { return timeInSeconds >= toCompare.timeInSeconds; }
	inline bool operator <(const TimeStamp& toCompare) const { return timeInSeconds < toCompare.timeInSeconds; }
	inline bool operator <=(const TimeStamp& toCompare) const { return timeInSeconds <= toCompare.timeInSeconds; }
	string toString()
	{
		string output = "";
		if (hours < 10) output += "0";
		output += allToString(hours) + "::";
		if (minutes < 10) output += "0";
		output += allToString(minutes) + "::";
		if (seconds < 10) output += "0";
		output+= allToString(seconds);
		return output;
	}
	void add(unsigned long timeToAdd)//timeToAdd is always in seconds
	{
		timeInSeconds += timeToAdd;
		setTimes();
	}
	void reset()
	{
		days = hours = minutes = seconds = timeInSeconds = 0;
	}
};