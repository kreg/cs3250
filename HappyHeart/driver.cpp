#include "HappyHeart.h"
#include <signal.h>

using namespace std;

int main()
{
	//prompt whether file will be used or not, with filename if file will be used
	//prompt whether output will be output to a tile or not, with filename if file will be used
	string filename;
	cout << "Welcome."
		<< "\nPlease input a filename with which I will receive input,"
		<< "\nor type \"no\" if you prefer to type input from the console\n";
	getline(cin, filename);
	//cin >> filename;
	istream *is = (filename == "no") ? &cin : new ifstream(filename);
	if (!(*is))//invalid file
	{
		cout << "Error, could not read filename! I will have to read from the console instead!\n";
		is = &cin;
	}

	cout << "Thank you."
		<< "\nMay I also have a file with which I might send the output?"
		<< "\n(Or, type \"no\" if you want the output right here.)\n";
	cin >> filename;
	ofstream outputStream;
	if (filename != "no") outputStream.open(filename.c_str());
	ostream& os = (filename == "no")? cout : outputStream;

	if (is == &cin)
	{
		cout << "Thank you. You may now begin entering patient input.\n"
			<< "The format is four numbers.\n"
			<< "The first represents a pulse, and is a whole number.\n"
			<< "The second represents oxygen percentage, and may use a decimal place.\n"
			<< "The third and fourth are systolic and diastolic blood pressure readings\n"
			<< "Do not put a slash between the blood pressure readings.\n"
			<< "End each input set with a non-numeric, non-whitespace character.\n"
			<< "An example input could be 80 94.7 120 80*\n";
	}

	HappyHeart myHeart((*is), os);

	outputStream.close();
	system("PAUSE");
	return 0;
}