#include <deque>
#include <list>

using namespace std;

template <typename T>
class StorageBehavior
{
public:
	virtual void add(T toadd) = 0;
	virtual T get() = 0;
	virtual void remove() = 0;
	virtual int size() = 0;
	virtual void clear() = 0;
protected:
};
template <typename T>
class Deque : public StorageBehavior<T>
{
private:
	deque<T>* data;
	Deque operator=(const Deque& x);
	Deque(const Deque& x);
public:
	~Deque() 
	{ 
		data->clear();
		delete data;
	}
	Deque()	{ data = new deque<T>(); }
	void add(T toadd) { data->push_back(toadd); }
	T get() { return data->front(); }
	void remove() { data->pop_front(); }
	int size() { return data->size(); }
	void clear() { data->clear(); }
};
template <typename T>
class List : public StorageBehavior<T>
{
private:
	list<T>* data;
	List operator=(const List& x);
	List(const List& x);
public:
	~List()
	{
		data->clear();
		delete data;
	}
	List() { data = new list<T>(); }
	void add(T toadd) { data->push_back(toadd); }
	T get() { return data->front(); }
	void remove() { data->pop_front(); }
	int size() { return data->size(); }
	void clear() { data->clear(); }
};

template <typename T>
class Queue
{
private:
	StorageBehavior<T>* data;
	Queue operator=(const Queue& x);
	Queue(const Queue& x);
public:
	Queue(StorageBehavior<T>* s) 
	{ 
		s->clear();
		data = s;
	}
	void add(T toadd) { data->add(toadd); }
	T get() { return data->get(); }
	void remove() { data->remove(); }
	int size() { return data->size(); }
	void clear() { data->clear(); }
	void changeImpl(StorageBehavior<T>* s)
	{
		s->clear();
		for (int i = 0; i < data->size(); ++i)
		{
			s->add(data->get());
			data->add(data->get());
			data->remove();
		}
		data = s;
	}
};