#include "classes.h"
#include <iostream>
#include <string>

using namespace std;

template <typename T>
void displayAndEmptyQueue(Queue<T>* s)
{
	for (int i = 0; i < s->size();)
	{
		cout << s->get() << " ";
		s->remove();
	}
	cout << endl;
}
int main()
{
	
   //// Test ints
   Deque<int> deq;
   Queue<int> q(&deq);
   q.add(1);
   q.add(2);
   
   // Swap implementation
   List<int> lst;
   lst.add(3);
   q.changeImpl(&lst);
   q.add(4);
   q.add(5);
   displayAndEmptyQueue(&q);

   // Test strings
   Deque<string> deq2;
   Queue<string> q2(&deq2);
   q2.add("1");
   q2.add("2");
   
   // Swap implementation
   List<string> lst2;
   lst2.add("3");
   q2.changeImpl(&lst2);
   q2.add("4");
   q2.add("5");
   displayAndEmptyQueue(&q2);

	return 0;
}