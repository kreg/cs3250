#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <map>
#include <sstream>

using namespace std;

//************************************************************************
//************************BASE OBSERVER CLASS*****************************
//************************************************************************
class StockObserver
{
public:
	virtual void notify() = 0;

protected:
};
//************************************************************************
//************************BASE REPORTER CLASS*****************************
//************************************************************************
class StockReporter
{
protected:
	list<StockObserver*>* observers;
	virtual void notifyObservers()
	{
		for ( list<StockObserver*>::iterator i = observers->begin(); i != observers->end(); i++)
		{
			(*i)->notify();
		}
	}
public:
	StockReporter()
	{
		observers = new list<StockObserver*>;
	}
	virtual ~StockReporter()
	{
		delete observers;
	}
	virtual void addObserver(StockObserver* toAdd)
	{
		observers->push_back(toAdd);
	}
	virtual void removeObserver(StockObserver* toRemove)
	{
		observers->remove(toRemove);//>remove(toRemove);
	}
};

//************************************************************************
//*****************************STOCK CLASS********************************
//************************************************************************
struct Stock
{
	string name, symbol;
	double curPrice, dollarChange, percChange, YTDpercChange, yrHigh, yrLow, PERatio;
};

//************************************************************************
//*************************STOCKREPORTER CLASS****************************
//************************************************************************
class LocalStocks : public StockReporter
{
public:
	LocalStocks(istream& is): stockAverage(0.0), stockStream(&is), timeStamp("") {}
	double getAverage();
	string getTime();
	void ReadDay();
	string getCompInfo(string compSymbol);
	string getTenPercentChange();

private:
	//member variables
	istream* stockStream;
	string timeStamp;
	string tenPercentChange;
	double stockAverage;
	map<string, Stock> stocks;

	//functions
	double calcAverage();
	string findTenPercentChange();

	//functions to help working with strings easier
	string trim(string toTrim);
	bool isNumber(char toTest);
	string getLastWord(string input);
};
//************************************************************************
//**********************DERIVED OBSERVER CLASSES**************************
//************************************************************************
class AverageObserver : public StockObserver
{
public:
	AverageObserver(LocalStocks& toObserve) : subject(&toObserve) 
	{
		subject->addObserver(this);
	}
	void notify()
	{
		cout << subject->getTime() << ", Average price: " << subject->getAverage() << "\n\n";
	}
private:
	LocalStocks* subject;
};
class TenPercObserver : public StockObserver
{
public:
	TenPercObserver(LocalStocks& toObserve) : subject(&toObserve)
	{
		subject->addObserver(this);
	}
	void notify()
	{
		cout << subject->getTime() << ":\n" << subject->getTenPercentChange() << endl;
	}
private:
	LocalStocks* subject;
};
class EightCompObserver : public StockObserver
{
public:
	EightCompObserver(LocalStocks& toObserve) : subject(&toObserve) 
	{
		subject->addObserver(this);
	}
	void notify()
	{
		cout << subject->getTime() << ":\n" << subject->getCompInfo("ALL") << endl <<  subject->getCompInfo("BA") << endl 
			<< subject->getCompInfo("BC") << endl << subject->getCompInfo("GBEL") << endl << subject->getCompInfo("KFT") << endl
			<< subject->getCompInfo("MCD") << endl << subject->getCompInfo("TR") << endl << subject->getCompInfo("WAG") << endl;
	}
private:
	LocalStocks* subject;
};