#include "classes.h"

int main()
{
	cout << "Please enter a file name with which you would like stock results read from: ";
	string filename;
	cin >> filename;
	ifstream is(filename.c_str());
	LocalStocks LS(is);

	AverageObserver avg(LS);

	LS.ReadDay();

	TenPercObserver tenp(LS);

	LS.ReadDay();

	EightCompObserver eco(LS);

	LS.ReadDay();
	LS.removeObserver(&eco);
	LS.ReadDay();
	LS.removeObserver(&tenp);
	LS.ReadDay();

	int extraneous = 1;
	extraneous += 25;
	return 0;
}