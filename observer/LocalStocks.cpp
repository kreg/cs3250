#include "classes.h"

// remove spaces from beginning and end of input string, and return the result
string LocalStocks::trim(string toTrim)
{
	if ((toTrim[0] != ' ')&&(toTrim[toTrim.size()-1] != ' ')) return toTrim;
	string retval = toTrim.substr(toTrim.find_first_not_of(' '));
	return retval.substr(0, retval.find_last_not_of(' ')+1);
}
// return true or false based on whether or not the input char was an ascii number 0-9
bool LocalStocks::isNumber(char toTest)
{
	if ((toTest >= 48)&&(toTest <= 57))	//ascii numbers range from 48 to 57 inclusive
		return true;
	return false;
}
// return the last word of the input string without spaces, "how are you   doing   " would return "doing"
string LocalStocks::getLastWord(string input)
{
	string toParse = trim(input);
	if (toParse == "") return "";
	int spacePos = toParse.find_last_of(' ');
	if (spacePos == string::npos) //not found
		return toParse;
	else return toParse.substr(spacePos+1);
}

//LocalStocks::LocalStocks(istream& is): stockAverage(0.0), stockStream(&is), timeStamp("") {}

void LocalStocks::ReadDay()
{
	stocks.clear();
	char inputChar;
	string compName, compSymbol, rest;
	char done = false;
	getline(*stockStream, timeStamp);
	timeStamp = timeStamp.substr(13);
	while (!done)
	{
		compName = compSymbol = "";
		if (stockStream->get(inputChar), inputChar == '\n')
		{
			done = true;
			break;
		}
		else stockStream->putback(inputChar);
		while (stockStream->get(inputChar))//this loop will grab the company's name/initials, and store them in companyName
		{
			if (!isNumber(inputChar)) compName += inputChar;
			else 
			{
				stockStream->putback(inputChar);
				break;
			}
		}
		compName = trim(compName);
		compSymbol = getLastWord(compName);
		compName = compName.substr(0, compName.length()-compSymbol.length());
		stocks[compSymbol].name = compName;
		stocks[compSymbol].symbol = compSymbol;
		*stockStream >> stocks[compSymbol].curPrice;
		*stockStream >> stocks[compSymbol].dollarChange;
		*stockStream >> stocks[compSymbol].percChange;
		*stockStream >> stocks[compSymbol].YTDpercChange;
		*stockStream >> stocks[compSymbol].yrHigh;
		*stockStream >> stocks[compSymbol].yrLow;
		//PERatio can apparently be nonexistent?
		if (*stockStream >> inputChar, inputChar == '-')
			stocks[compSymbol].PERatio = 0.0;
		else
		{
			stockStream->putback(inputChar);
			*stockStream >> stocks[compSymbol].PERatio;
		}
		getline(*stockStream, rest);
	}
	stockAverage = calcAverage();
	tenPercentChange = findTenPercentChange();
	notifyObservers();
}
string LocalStocks::getCompInfo(string compSymbol)
{
	stringstream out;
	out << "Stock name: " << stocks[compSymbol].name << "\nSymbol: " << stocks[compSymbol].symbol << "\nCurrent Price: " 
		<< stocks[compSymbol].curPrice << "\nDollar Change: " << stocks[compSymbol].dollarChange << "\nPercent Change: "
		<< stocks[compSymbol].percChange << "\nYTD percent change: " << stocks[compSymbol].YTDpercChange << "\n52-week high: "
		<< stocks[compSymbol].yrHigh << "\n52-week low: " << stocks[compSymbol].yrLow << "\nPE ratio: " << stocks[compSymbol].PERatio 
		<< '\n';
	return out.str();
}
double LocalStocks::calcAverage()
{
	//std::_Tree_iterator<std::_Tree<std::_Tmap_traits<std::string, Stock, std::less<std::string>, std::allocator<std::pair<const std::string, Stock>>, false>>::_Mybase> i;
	double total = 0.0;
	for (auto i = stocks.begin(); i != stocks.end(); i++)
	{
		total += i->second.curPrice;
	}
	return total/stocks.size();
}
double LocalStocks::getAverage()
{
	return stockAverage;
}
string LocalStocks::findTenPercentChange()
{
	stringstream out;
	for (auto i = stocks.begin(); i != stocks.end(); i++)
	{
		if ((i->second.percChange >= 10.0)||(i->second.percChange <= -10.0))
			out << i->second.symbol << ' ' << i->second.curPrice << ' ' << i->second.percChange << endl;
	}
	return out.str();
}
string LocalStocks::getTenPercentChange()
{
	return tenPercentChange;
}
string LocalStocks::getTime()
{
	return timeStamp;
}