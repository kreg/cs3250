#include <iostream>
#include <fstream>
#include "decorators.h"
#include "StreamOutput.h"

using namespace std;

bool containsNumber (string s)
{
	for (size_t i = 0; i < s.length(); ++i)
	{//numbers are 48 to 57 inclusive
		if (s[i] >= 48 && s[i] <= 57)//it's a number
			return true;
	}
	return false;
}
bool containsCap (string s)
{
	for (size_t i = 0; i < s.length(); ++i)//65 to 90 inclusive
		if (s[i] >= 65 && s[i] <= 90)//has a capitalizedl letter
			return true;
	return false;
}

int main()
{
	bool done = false;
	bool started = false;

	cout << "Please input the file you would like me to read from: ";
	string filename;
	getline (cin, filename);
	ifstream is(filename.c_str());
	if (!is)
	{
		cout << "I'm sorry, I was unable to work with the filename you gave me. Goodbye";
		done = true;
	}

	Output<string>* SO = new StreamOutput<string>(cout);
	char input;

	while (!done)
	{
		if (!started) cout << "Thank you. We will now decide how to decorate your file. Please select from the following options:\n";
		if (started) cout << "\n\n\nThank you for your selection. Feel free to continue decorating the file. ";

		cout << "Here are the options again for your convenience:\n"		
			<< "Press 1 to decorate the output with a LineOutput decorator.\n" 
			<< "Press 2 to decorate the output with a NumberedOutput decorator.\n"
			<< "Press 3 to decorate the output with a TeeOutput decorator.\n"
			<< "Press 4 to decorate the output with a Scream decorator.\n"
			<< "Press 5 to decorate the output with a LEET decorator.\n"
			<< "Press 6 to decorate the output with a deLEET decorator.\n"
			<< "Press 7 to decorate the output with a double dutch decorator.\n"
			<< "Press 8 to decorate the output with an opish decorator.\n"
			<< "Press 9 if you are done decorating, and would like to end.\n";
		started = true;
		cin >> input;
		if (input == '1')//lineOutput
			SO = new LineOutput<string>(SO);
		else if (input == '2')//NumberedOutput
			SO = new NumberedOutput<string>(SO);
		else if (input == '3')//TeeOutput
		{
			cout << "\nPlease specify an additional file you would like me to write to with your TeeOutput filter: ";
			string fn;
			cin >> input;
			cin.putback(input);
			getline(cin, filename);
			ofstream* os = new ofstream;
			os->open(filename.c_str(), ios::trunc);
			SO = new TeeOutput<string>(os, SO);
		}
		else if (input == '4')//Scream
		{
			SO = new Scream<string>(SO);
		}
		else if (input == '5')//LEET
		{
			SO = new LEET<string>(SO);
		}
		else if (input == '6')//deLEET
		{
			SO = new deLEET<string>(SO);
		}
		else if (input == '7')//DoubleDutch
		{
			SO = new DoubleDutch<string>(SO);
		}
		else if (input == '8')//Opish
		{
			SO = new Opish<string>(SO);
		}
		else if (input == '9')//Showtime
		{
			string fileInput;
			while (getline(is, fileInput))
			{
				SO->writeString(fileInput);
			}
		break;
		}
		else cout << "Error, that was an invalid entry. Please enter a number between 1 and 5.\n";
	}
	system("PAUSE");
	delete SO;
	return 0;
}