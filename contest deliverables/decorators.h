#include "output.h"
#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <sstream>

using namespace std;

template <class T>
string allToStr(T s)
{
	stringstream out;
	out << s;
	return out.str();
};

template <typename T>
class LineOutput : public Output<T>
{
private:
	Output<T>* wrapped;
public:
	LineOutput(Output<T>* toWrap) : wrapped(toWrap) {}
	~LineOutput()
	{
		delete wrapped;
	}
    void write(const T& toWrite)
	{
		wrapped->write(toWrite);
		wrapped->writeString("\n");
	}
    void writeString(const std::string& toWrite)
	{
		wrapped->writeString(toWrite + "\n");
	}
};
template <typename T>
class NumberedOutput : public Output<T>
{
private:
	Output<T>* wrapped;
	int lineNumber;
public:
	NumberedOutput(Output<T>* toWrap) :wrapped(toWrap), lineNumber(0)	{	}
	~NumberedOutput()
	{
		delete wrapped;
	}
	void write (const T& toWrite)
	{
		wrapped->writeString(allToStr(++lineNumber) + ": ");
		wrapped->write(toWrite);
		wrapped->writeString("\n");
	}
	void writeString(const std::string& toWrite)
	{
		wrapped->writeString(allToStr(++lineNumber) + "\n" + toWrite);
	}
};

template <typename T>
class TeeOutput : public Output<T>
{
private:
	ofstream* os2;
	Output<T>* wrapped;
public:
	~TeeOutput()
	{
		delete wrapped;
		os2->close();
	}
	TeeOutput(ofstream* os, Output<T>* toWrap) : wrapped(toWrap), os2(os) {}
	void write(const T& toWrite)
	{
		(*os2) << toWrite;
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		(*os2) << toWrite;
		wrapped->writeString(toWrite);
	}
};
template<typename T>
class Scream : public Output<T>
{
private:
	Output<T>* wrapped;
	string toCaps(string s)
	{
		for (size_t i = 0; i < s.length(); ++i)
			if (s[i] >= 97 && s[i] <= 122)//97 to 122 inclusive are the lower-case letters
				s[i] -= 32;
		return s;
	}
public:
	~Scream()
	{
		delete wrapped;
	}
	Scream(Output<T>* toWrap) : wrapped(toWrap){}
	void write(const T& toWrite)//this decorator will only modify the writeString function
	{
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		wrapped->writeString(toCaps(toWrite));
	}
};

template <typename T>
class LEET : public Output<T>
{
private:
	Output<T>* wrapped;
	string toLEET(string s)
	{
		for (size_t i = 0; i < s.length(); ++i)
		{
			if (s[i] == 97 || s[i] == 65) s[i] = 64;	  //A->@
			else if (s[i] == 98 || s[i] == 66) s[i] = 56; // B->8
			else if (s[i] == 99 || s[i] == 67) s[i] = 40; // C->(
			else if (s[i] == 101 || s[i] == 69) s[i] = 51; // E->3
			else if (s[i] == 103 || s[i] == 71) s[i] = 57; // G->9
			else if (s[i] == 104 || s[i] == 72) s[i] = 35; // H->#
			else if (s[i] == 105 || s[i] == 73) s[i] = 33; // I->!
			else if (s[i] == 108 || s[i] == 76) s[i] = 49; // L->1
			else if (s[i] == 111 || s[i] == 79) s[i] = 48; // O->0
			else if (s[i] == 115 || s[i] == 83) s[i] = 53; // S->5
			else if (s[i] == 116 || s[i] == 84) s[i] = 43; // T->+
			else if (s[i] == 122 || s[i] == 90) s[i] = 50; // Z->2
		}
		return s;
	}
public:
	~LEET()
	{
		delete wrapped;
	}
	LEET(Output<T>* toWrap) : wrapped(toWrap){}
	void write(const T& toWrite)//this decorator will only modify the writeString function
	{
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		wrapped->writeString(toLEET(toWrite));
	}
};
//note, this class will not work perfectly, but should mostly make the file readable if it has previously
//been run though the LEET decorator
template <typename T>
class deLEET : public Output<T>
{
private:
	Output<T>* wrapped;
	string unLEET(string s)
	{
		for (size_t i = 0; i < s.length(); ++i)
		{
			if (s[i] == 64) s[i] = 65; //@->a
			else if (s[i] == 56) s[i] = 66; //8->b
			else if (s[i] == 40) s[i] = 67; //(->c
			else if (s[i] == 51) s[i] = 69; //3->e
			else if (s[i] == 57) s[i] = 71; //9->g
			else if (s[i] == 35) s[i] = 72; //#->h
			else if (s[i] == 33) s[i] = 73; //!->i
			else if (s[i] == 49) s[i] = 76; //1->l
			else if (s[i] == 48) s[i] = 79; //0->o
			else if (s[i] == 52) s[i] = 83; //5->s
			else if (s[i] == 43) s[i] = 84; //+->t
			else if (s[i] == 50) s[i] = 90; //2->z
		}
		return s;
	}
public:
	~deLEET()
	{
		delete wrapped;
	}
	deLEET(Output<T>* toWrap) : wrapped(toWrap) {}
	void write(const T& toWrite)//this decorator will only modify the writeString function
	{
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		wrapped->writeString(unLEET(toWrite));
	}
};
template <typename T>
class DoubleDutch : public Output<T>
{
private:
	Output<T>* wrapped;
	string Dutchify(string s)
	{
		string retval = "";
		for (size_t i = 0; i < s.length(); ++i)
		{
			retval += s[i];
			if (s[i] == 'b' || s[i] == 'B') retval += s[i] + "ub";
			else if (s[i] == 'c' || s[i] == 'C') retval += "ash";
			else if (s[i] == 'd' || s[i] == 'D') retval += "ud";
			else if (s[i] == 'f' || s[i] == 'F') retval += "uf";
			else if (s[i] == 'g' || s[i] == 'G') retval += "ug";
			else if (s[i] == 'h' || s[i] == 'H') retval += "ash";
			else if (s[i] == 'j' || s[i] == 'J') retval += "ay";
			else if (s[i] == 'k' || s[i] == 'K') retval += "uck";
			else if (s[i] == 'l' || s[i] == 'L') retval += "ul";
			else if (s[i] == 'm' || s[i] == 'M') retval += "um";
			else if (s[i] == 'n' || s[i] == 'N') retval += "un";
			else if (s[i] == 'p' || s[i] == 'P') retval += "up";
			else if (s[i] == 'q' || s[i] == 'Q') retval += "ack";
			else if (s[i] == 'r' || s[i] == 'R') retval += "ug";
			else if (s[i] == 's' || s[i] == 'S') retval += "us";
			else if (s[i] == 't' || s[i] == 'T') retval += "ut";
			else if (s[i] == 'v' || s[i] == 'V') retval += "uv";
			else if (s[i] == 'w' || s[i] == 'W') retval += "ack";
			else if (s[i] == 'x' || s[i] == 'X') retval += "ux";
			else if (s[i] == 'y' || s[i] == 'Y') retval += "uck";
			else if (s[i] == 'z' || s[i] == 'Z') retval += "ub";
		}
		return retval;
	}
public:
	~DoubleDutch()
	{
		delete wrapped;
	}
	DoubleDutch(Output<T>* toWrap) : wrapped(toWrap) {}
	void write(const T& toWrite)//this decorator will only modify the writeString function
	{
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		wrapped->writeString(Dutchify(toWrite));
	}
};
template <typename T>
class Opish : public Output<T>
{
private:
	Output<T>* wrapped;
	string Opify(string s)
	{
		string retval = "";
		for (size_t i = 0; i < s.length(); ++i)
		{
			retval += s[i];
			if ((s[i] >= 65 && s[i] <=90)||(s[i] >= 97 && s[i] <= 122))//it's a letter
				if (s[i] != 'a' && s[i] != 'A' && s[i] != 'e' && s[i] != 'E' && s[i] != 'i' && s[i] != 'I' && s[i] != 'o' && s[i] != 'O' && s[i] != 'u' && s[i] != 'U') retval += "op";
		}
		return retval;
	}
public:
	~Opish()
	{
		delete wrapped;
	}
	Opish(Output<T>* toWrap) : wrapped(toWrap) {}
	void write(const T& toWrite)//this decorator will only modify the writeString function
	{
		wrapped->write(toWrite);
	}
	void writeString(const string& toWrite)
	{
		wrapped->writeString(Opify(toWrite));
	}
};