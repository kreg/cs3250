﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace PatsTracks
{
    public class CustomerDB
    {
        List<KeyValuePair<int, PatsCustomer>> customerList;
        public CustomerDB()
        {
            customerList = new List<KeyValuePair<int, PatsCustomer>>();
            customerList = customerList.OrderBy(x => x.Value.Name).ToList();
        }
        public void AddOrUpdateCustomer(PatsCustomer cust)
        {
            foreach (var i in customerList)
                if (i.Key == cust.CustomerKey)
                {
                    customerList.Remove(i);
                    customerList.Add(new KeyValuePair<int, PatsCustomer>(cust.CustomerKey, cust));
                    return;
                }
            customerList.Add(new KeyValuePair<int, PatsCustomer>(cust.CustomerKey, cust));
        }
        public int getHighestKey()
        {
            int temp = 0;
            foreach (var i in customerList)
            {
                if (i.Key > temp) temp = i.Key;
            }
            return temp;
        }
        public List<PatsCustomer> getAllCustomers()
        {
            List<PatsCustomer> retval = new List<PatsCustomer>();
            foreach (var i in customerList)
                retval.Add(i.Value);
            retval = retval.OrderBy(x => x.Name).ToList();
            return retval;
        }
        public PatsCustomer Find(int custKey)
        {
            foreach (var i in customerList)
                if (custKey == i.Key)
                    return i.Value;
            throw new Exception("Customer did not exist");
        }
        public void Save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream("Customers.dat", FileMode.OpenOrCreate, FileAccess.Write);
            bf.Serialize(fs, customerList);
            fs.Close();
        }
        public void Load()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream("Customers.dat", FileMode.Open, FileAccess.Read);
            customerList = (List<KeyValuePair<int, PatsCustomer>>)bf.Deserialize(fs);
            fs.Close();
        }
    }
    [Serializable]
    public class PatsCustomer
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        int custKey;
        //these will be KeyValuePairs of media, with their serial numbers
        MediaDB boughtMedia, soldMedia;

        public PatsCustomer(string name, string phone, string address, int custKey)
        {
            this.Name = name;
            this.Phone = phone;
            this.Address = address;
            this.custKey = custKey;
            boughtMedia = new MediaDB();
            soldMedia = new MediaDB();
        }
        public override string ToString()
        {
            return "Name: " + Name + ", ID: " + custKey;
        }
        public List<Media> getAllSales()
        {
            return soldMedia.getAllMedia();
        }
        public List<Media> getAllPurchases()
        {
            return boughtMedia.getAllMedia();
        }
        public void buyItem(ref Media toAdd)
        {
            boughtMedia.AddOrUpdateMedia(ref toAdd);
        }
        public void sellItem(ref Media toAdd)
        {
            soldMedia.AddOrUpdateMedia(ref toAdd);
        }
        public int CustomerKey
        {
            get { return custKey; }
        }
    }
}
