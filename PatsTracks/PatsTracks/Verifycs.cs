﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PatsTracks
{
    public class Verify
    {

        public static string verifyCustomerData(string name, string address, string phone)
        {
            Regex phoneExp = new Regex( @"^\d{10}$" );
            if (name != "" && address != "" && phone != "")
            {
                if (phoneExp.Match(phone).Success == false)
                    return "bad";
                return "good";
            }
            return "bad";
        }
        public static string verifyMediaData(string title, string artist, string genre, string decade, double purchasePrice)
        {
            if (title != "" && artist != "" && genre != "" && decade != "" && purchasePrice >= 0)
                return "good";
            return "bad";
        }
    }
}
