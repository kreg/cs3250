﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatsTracks
{
    public class PatsTracksMain
    {
        CustomerDB customers;
        MediaDB allMedia;
        private int nextMediaSerialNumber, nextCustKey;
        public int lastMediaSN
        {
            get { return nextMediaSerialNumber - 1; }
        }
        public int lastCustID
        {
            get { return nextCustKey - 1; }
        }
        public List<Media> getAllMedia()
        {
            return allMedia.getAllMedia();
        }
        public Media findMedia(int sn)
        {
            try
            {
                return allMedia.Find(sn);
            }
            catch
            {
                throw new Exception("Could not find media");
            }
        }
        public PatsCustomer findCustomer(int ID)
        {
            return customers.Find(ID);
        }
        public List<PatsCustomer> getAllCustomers()
        {
            return customers.getAllCustomers();
        }
        public PatsTracksMain()
        {
            customers = new CustomerDB();
            allMedia = new MediaDB();
            nextMediaSerialNumber = nextCustKey = 1;
        }
        public void Save()
        {
            customers.Save();
            allMedia.Save();
        }
        public void Load()
        {
            customers.Load();
            allMedia.Load();
            //make sure the keys get set correctly when the program starts
            nextCustKey = customers.getHighestKey() + 1;
            nextMediaSerialNumber = allMedia.getHighestKey() + 1;
        }
        public string addOrUpdateCustomer(string name, string phone, string address, int ID = 0)
        {
            if (Verify.verifyCustomerData(name, address, phone) == "good")
            {
                PatsCustomer toAdd;
                if (ID != 0)
                    toAdd = new PatsCustomer(name, phone, address, ID);
                else
                {
                    foreach (PatsCustomer c in getAllCustomers())
                    {
                        if (c.Name == name)
                            if (c.Phone == phone)
                                if (c.Address == address)
                                    return "Exact user already exists!";
                    }
                    toAdd = new PatsCustomer(name, phone, address, nextCustKey);
                    ++nextCustKey;
                }
                customers.AddOrUpdateCustomer(toAdd);
                return "Customer added successfully";
            }
            return "Error, could not add customer, please make sure input was valid!";
        }
        public string buyOrUpdateMedia(string title, string artist, string genre, string decade, double purPrice, double salePrice = 0, int SN = 0)
        {
            if (Verify.verifyMediaData(title, artist, genre, decade, purPrice) == "good")
            {
                Media toAdd = null;
                if (SN == 0)//then add
                {
                    toAdd = new Media(title, artist, genre, decade, nextMediaSerialNumber, purPrice, false, 0.0);
                    allMedia.AddOrUpdateMedia(ref toAdd);
                    nextMediaSerialNumber++;
                }
                else//update
                {
                    try
                    {
                        toAdd = allMedia.Find(SN);
                    }
                    catch { }
                    toAdd.Title = title;
                    toAdd.Artist = artist;
                    toAdd.Genre = genre;
                    toAdd.Decade = decade;
                    toAdd.PurchasePrice = purPrice;
                    if (toAdd.sold)
                        toAdd.SalePrice = salePrice;
                }
                return "Media successfully purchased!";
            }
            return "Error, could not add media, please make sure input was valid and try again!";
        }
        public string sellMedia(int mediaKey, double sellPrice, int sellerKey)
        {
            try
            {
                PatsCustomer seller = customers.Find(sellerKey);
                Media toSell = allMedia.Find(mediaKey);
                seller.buyItem(ref toSell);
                toSell.sold = true;
                toSell.SalePrice = sellPrice;
                return "Media successfully sold!";
            }
            catch
            {
                return "Error could not sell item. Did the media and customer exist?";
            }
        }
        public string buyMedia(int mediaKey, double buyPrice, int sellerKey)
        {
            try
            {
                PatsCustomer buyer = customers.Find(sellerKey);
                Media toBuy = allMedia.Find(mediaKey);
                buyer.sellItem(ref toBuy);
                toBuy.PurchasePrice = buyPrice;
                return "Media successfully bought!";
            }
            catch
            {
                return "Error, could not buy the item. Did the media and customer exist?";
            }
        }
    }
}
